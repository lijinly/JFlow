package bp.ccbill.template;

import bp.da.*;
import bp.en.*;
import bp.wf.port.*;
import bp.ccbill.*;
import java.util.*;

/** 
 单据可创建的工作岗位属性	  
*/
public class StationCreateAttr
{
	/** 
	 单据
	*/
	public static final String FrmID = "FrmID";
	/** 
	 工作岗位
	*/
	public static final String FK_Station = "FK_Station";
}
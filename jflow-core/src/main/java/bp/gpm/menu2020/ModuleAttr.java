package bp.gpm.menu2020;

import bp.en.EntityTreeAttr;

/** 
	 模块
*/
	public class ModuleAttr extends EntityTreeAttr
	{
		/** 
		 系统编号
		*/
		public static final String SystemNo = "SystemNo";
		/** 
		 标记
		*/
		public static final String Remark = "Remark";
		/** 
		 组织编号
		*/
		public static final String OrgNo = "OrgNo";
		/** 
		 Icon
		*/
		public static final String Icon = "Icon";
	}
